//
//  TUContants.swift
//  ThankYou
//
//  Copyright © 2015 A Simple Thank You. All rights reserved.
//

import Foundation
import UIKit
import CoreData


var VerifyEmailMobileUserObjectDict : NSDictionary = [:]

var SubmitForgotPasswordResponseObjectDict : NSDictionary = [:]

var DOCUMENT_TAB_OPEN : Bool = false

var ViewUserDetailsDict : NSDictionary = [:]

var DocumentDetailsList : NSArray = []

var ViewGalleryDict : NSDictionary = [:]

var ViewHotelDictList : NSMutableArray = []

var PlaceToVisitList : NSMutableArray = []

var HotelImagePath : NSMutableArray = []

var CheckInTimeArry : NSMutableArray = []

var StateListDict : NSArray = []

var CityListDict : NSArray = []

var GalleryDetailDict : NSMutableArray = []

var StartDateArray : NSMutableArray = []

var photoUploadDate : NSMutableArray = []

var EventNameObj : NSMutableArray = []

var UploadedByObj : NSMutableArray = []

let IS_IPHONE_5 = (fabs(Double(UIScreen.main.bounds.size.height) - Double(568)) < DBL_EPSILON)
let IS_IPHONE_6 = (fabs(Double(UIScreen.main.bounds.size.height) - Double(667)) < DBL_EPSILON)
let IS_IPHONE_6Plus = (fabs(Double(UIScreen.main.bounds.size.height) - Double(736)) < DBL_EPSILON)

var CSVList :[CSVModel] = []

var SELECTED_INDEX = 0

var CURRENT_PROGESS_BAR : Float = 0.0

var task: URLSessionDownloadTask!
var session: URLSession!
var cache:NSCache<AnyObject, AnyObject>!


var VIEW_PROFILE_FLAG = false

let SERVER_IP = "s3.ap-south-1.amazonaws.com"

let BASE_URL = "http://\(SERVER_IP)/elsevr/"

let API_CSV_FILE4K = BASE_URL + "elseVR4K.csv"

let API_CSV_FILE2K = BASE_URL + "elseVR4K.csv"


let API_HTML_TEXT = BASE_URL + "article_html/"



let NO_INTERNET_MSG = "Check your internet connection!"





let API_RESET_PASSWORD = BASE_URL + "ResetPassword"

let API_CHANGE_PASSWORD = BASE_URL + "auth"

let API_RESET_PASSWORD_OTP = BASE_URL + "SubmitForgotPassword"

let API_VERIFY_OTP = BASE_URL + "VerifyUserOtp"

let API_REGISTER_USER = BASE_URL + "RegisterUser"

let API_UPDATE_PROFILE_USER = BASE_URL + "auth"

let API_VERIFY_EMAILMOBILE = BASE_URL + "VerifyEmailAndMobile"

let API_VERIFY_OTP_REQUEST = BASE_URL + "VerifyUserOtp"

let API_GET_EVENT_LIST = BASE_URL + "auth"

let API_GET_EVENT_DETAILS = BASE_URL + "auth"

let API_GET_HELP_SUPPORT = BASE_URL + "auth"

let API_GET_ABOUT_EVENTS = BASE_URL + "AboutEven"

let API_ADD_GALLERY_PHOTOS = BASE_URL + "auth"

let API_GET_GAllARY_SUPPORT = BASE_URL + "auth"

let API_GET_ITINERARY = BASE_URL + "auth"

let API_GET_HOTELS = BASE_URL + "auth"

let API_GET_TICKETS = BASE_URL + "auth"

let API_GET_THINGS_TO_DO = BASE_URL + "auth"

let API_GET_SOS = BASE_URL + "auth"

let API_GET_PROFILE_PHOTO = BASE_URL + "auth"

let API_GET_LIKE = BASE_URL + "auth"


let API_LOCATION_LOGO = BASE_URL + "getLocationLogoImage"
let API_GETSERVICE_STATUS = BASE_URL + "getService"
let API_REPLACE_DEVICE_ID = BASE_URL + "replaceDeviceIds"
let API_UPLOAD_BACKUP_FILE = BASE_URL + "uploadBackupFile"
//tushar end

let API_VERSE_LIST = BASE_URL + "verseList"
let API_BOOK_TYPE_LIST = BASE_URL + "bookTypeList"
let API_APP_BACKGROUND_LIST = BASE_URL + "appBackgroundsList"
let API_POR_LIST = BASE_URL + "placeOfRestList"
let API_SERVICE_LIST = BASE_URL + "serviceList"

let API_APP_BACKGROUND_IMAGE = BASE_URL + "appBackgroundImage"
let API_APP_CREATE_SERVICE = BASE_URL + "createService"
let API_APP_DELETE_SERVICE = BASE_URL + "deleteService"
let API_APP_END_SERVICE = BASE_URL + "endService"
let API_APP_RUN_SERVICE = BASE_URL + "runService"
let API_APP_ADD_GUEST = BASE_URL + "addGuest"
let API_APP_DELETE_GUEST = BASE_URL + "deleteGuest"
let API_APP_UPLOAD_SERVICE_IMAGES = BASE_URL + "uploadServicePhotos"
let API_APP_UPDATE_DEVICE_INFO = BASE_URL + "updateDeviceInfo"

let API_APP_GET_SERVICE_PHOTO = BASE_URL + "getServicePhoto?"
let API_APP_GET_LOCATION_LOGO = BASE_URL + "locationSetting"

let API_APP_UPLOAD_DB_FILE = BASE_URL + "uploadBackupFile"


let CREATED_FROM_IPAD = "iPad"
let CREATED_FROM_ONLINE = "Online"

let DATE_FORMAT = "MM-dd-yyyy"
let TIME_FORMAT = "hh:mm a"

let DATE_TIME_FORMAT = "MM-dd-yyyy hh:mm a"
let DATE_TIME_FORMAT_SERVER = "yyyy-MM-dd'T'HH:mm:ssZ"

let SERVICE_STATUS_NEW_ENUM = "NEW"
let SERVICE_STATUS_RUNNING_ENUM = "RUNNING"
let SERVICE_STATUS_ENDED_ENUM = "ENDED"

let SERVICE_STATUS_NEW = "New"
let SERVICE_STATUS_RUNNING = "Running"
let SERVICE_STATUS_ENDED = "Ended"

let APP_BACKGROUND_IMAGES = "AppBackgrounds"
let SERVICE_IMAGES = "ServiceImages"
let LOCATION_LOGO_IMAGE = "Locationimage"

let SERVICE_APP_BACKGROUND_OPTION_CUSTOM = 3

let DEFAULT_OPT_IN_MESSAGE = "By checking this box, I give you permission to contact me at the address or email listed above"

let ERROR_EMAIL = "Please enter valid email"




