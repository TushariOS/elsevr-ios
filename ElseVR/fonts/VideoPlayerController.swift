//
//  VideoPlayerController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 07/03/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import MZDownloadManager
import AVFoundation
class VideoPlayerController: UIViewController ,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var videoVRView: GVRVideoView!
    @IBOutlet weak var SeekBar: UISlider!
    @IBOutlet weak var MuteBtn: UIButton!
    @IBOutlet weak var PlayBtn: UIButton!
    @IBOutlet weak var TimeFrameLbl: UILabel!
    @IBOutlet weak var BackBtn: UIButton!
    @IBOutlet weak var RestartBtn: UIButton!
    
    @IBOutlet weak var backgroungImage: UIImageView!
    @IBOutlet weak var DeviceOrientationImg: UIImageView!
    @IBOutlet weak var RotateDeviceLbl: UILabel!
    @IBOutlet weak var HeadphoneImg: UIImageView!
    @IBOutlet weak var UseHeadphoneLbl: UILabel!
    
    let myDownloadPath = MZUtility.baseFilePath + "/My Videos"
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    var currentView: UIView?
    var isMute = false
    var isPlaying = true
    var isFullScreenClear = false
    var backgroundImage: UIImage!
    let backButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.AllowVolumeAtSilentButtonOn()
        self.AnimatingStartingLabel()
        self.CreateBackBtn()
        
        self.backgroungImage.image = backgroundImage
        self.backgroungImage.contentMode = .scaleAspectFill
        self.navigationController?.isNavigationBarHidden = true
        
        let Viewtap = UITapGestureRecognizer(target: self, action: #selector(self.tappedOnView(_:)))
        Viewtap.delegate = self
        videoVRView.addGestureRecognizer(Viewtap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoPlayerController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
    }
    
    
    func CreateBackBtn(){
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named:"back_arrow.png"), for: UIControlState.normal)
        
        if(IS_IPHONE_5){
            
            backButton.frame = CGRect(x:290, y:15, width:20,height: 30)
            
        }else if(IS_IPHONE_6){
            
            backButton.frame = CGRect(x:345, y:15, width:20,height: 30)
            
        }else if (IS_IPHONE_6Plus){
            
            backButton.frame = CGRect(x:385, y:15, width:20,height: 30)
            
        }
        backButton.addTarget(self, action: #selector(DetailsViewController().backButtonPressed(sender:)), for:.touchUpInside)
        self.view.addSubview(backButton)
        
    }
    
    func backButtonPressed(sender: UIButton!) {
        
        let allVC = self.navigationController?.viewControllers
        for index  in 0..<allVC!.count {
            
            if  let ListVC = allVC![index] as? DetailsViewController {
                self.navigationController!.popToViewController(ListVC, animated: false)
            }
            
        }
        
    }
    
    func AnimatingStartingLabel(){
        
        self.videoVRView.isHidden =                  true
        self.SeekBar.isHidden =                      true
        self.MuteBtn.isHidden =                      true
        self.PlayBtn.isHidden =                      true
        self.TimeFrameLbl.isHidden =                 true
        self.RestartBtn.isHidden =                   true
        
        self.DeviceOrientationImg.frame =            CGRect(x:120,y:182,width:80,height:40)
        self.RotateDeviceLbl.frame =                 CGRect(x:75,y:230,width:170,height:30)
        self.HeadphoneImg.frame =                    CGRect(x:140,y:320,width:40,height:40)
        self.UseHeadphoneLbl.frame =                 CGRect(x:90,y:368,width:140,height:50)
        
        self.DeviceOrientationImg.isHidden =         true
        self.RotateDeviceLbl.isHidden =              true
        self.HeadphoneImg.isHidden =                 true
        self.UseHeadphoneLbl.isHidden =              true
        
        UIView.animate(withDuration: 0.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.DeviceOrientationImg.alpha =         0.0
            self.RotateDeviceLbl.alpha =              0.0
            self.HeadphoneImg.alpha =                 0.0
            self.UseHeadphoneLbl.alpha =              0.0
            
        }, completion: {
            (finished: Bool) -> Void in
            self.DeviceOrientationImg.isHidden =      false
            self.RotateDeviceLbl.isHidden =           false
            self.HeadphoneImg.isHidden =              false
            self.UseHeadphoneLbl.isHidden =           false
            
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.DeviceOrientationImg.alpha =     1.0
                self.RotateDeviceLbl.alpha =          1.0
                self.HeadphoneImg.alpha =             1.0
                self.UseHeadphoneLbl.alpha =          1.0
                
            }, completion: {
                (finished: Bool) -> Void in
                
            })
        })
        
    }
    
    func rotated(){
        
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft{
            
            self.DeviceOrientationImg.isHidden =         true
            self.RotateDeviceLbl.isHidden =              true
            self.HeadphoneImg.isHidden =                 true
            self.UseHeadphoneLbl.isHidden =              true
            self.backgroungImage.isHidden =              true
            self.backButton.isHidden =                   true
            
            self.videoVRView.isHidden =                  false
            self.SeekBar.isHidden =                      false
            self.MuteBtn.isHidden =                      false
            self.PlayBtn.isHidden =                      false
            self.TimeFrameLbl.isHidden =                 false
            self.RestartBtn.isHidden =                   false
            
            self.loadVideo()
            
        }
    }
   
    func loadVideo(){
        
        videoVRView.isHidden = true
        RestartBtn.isHidden = true
        self.TimeFrameLbl.text = "Loading video..."
        
        
        //let localPath = MZUtility.baseFilePath + "/My Videos/" + "\(CSVList[SELECTED_INDEX].id).mp4"
        let localPath = myDownloadPath.appending("/file1" + ".mp4")
        
        if  let path =  Bundle.main.path(forResource:"6", ofType:"mp4") {
            print(localPath)
            let fileUrl = URL(fileURLWithPath: path)
            
            self.videoVRView.load(from: fileUrl, of: GVRVideoType.mono)
            self.videoVRView.enableCardboardButton = false
            self.videoVRView.enableFullscreenButton = false
            self.videoVRView.enableInfoButton = false
            self.videoVRView.enableTouchTracking = true
            self.videoVRView.delegate = self
            
        }
        
    }
    func tappedOnView(_ gestureRecognizer: UITapGestureRecognizer) {
       // myView.alpha = 0.5
        
        if(isFullScreenClear == true){
            
            isFullScreenClear = false
            
            self.SeekBar.isHidden =         true
            self.MuteBtn.isHidden =         true
            self.PlayBtn.isHidden =         true
            self.TimeFrameLbl.isHidden =    true
            self.BackBtn.isHidden =         false
      
        }else{
            
            isFullScreenClear = true
            
            self.SeekBar.isHidden =         false
            self.MuteBtn.isHidden =         false
            self.PlayBtn.isHidden =         false
            self.TimeFrameLbl.isHidden =    false
            self.BackBtn.isHidden =         false
        
        
        }
        
        
    }
    
    func AllowVolumeAtSilentButtonOn(){
    
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    
    }
  

    @IBAction func BackButtonClicked(_ sender: UIButton) {
        
        let allVC = self.navigationController?.viewControllers
        for index  in 0..<allVC!.count {
            
            if  let ListVC = allVC![index] as? DetailsViewController {
                self.navigationController!.popToViewController(ListVC, animated: false)
            }
            
        }

        
    }
    
    @IBAction func MuteBtnClicked(_ sender: UIButton) {
        
        if(isMute == false){
            isMute = true
            self.MuteBtn.setImage(UIImage(named:"soundoff_btn.png"), for: UIControlState.normal)
            self.videoVRView.volume = 0
            
        
        }else{
            
            isMute = false
            self.MuteBtn.setImage(UIImage(named:"soundon_btn.png"), for: UIControlState.normal)
            self.videoVRView.volume = 1
        
        }
        
        
    }
    
    @IBAction func PlayBtnClicked(_ sender: UIButton) {
        
        if(isPlaying == true){
            
            isPlaying = false
            self.PlayBtn.setImage(UIImage(named:"play_btn.png"), for: UIControlState.normal)
            self.videoVRView.pause()
            
        }else{
            
            isPlaying = true
            self.PlayBtn.setImage(UIImage(named:"pause_btn.png"), for: UIControlState.normal)
            self.videoVRView.play()
            
        }
        
        
    }
  
    @IBAction func SeekBarvalueChangedAction(_ sender: UISlider) {
        
        self.videoVRView.pause()
        self.videoVRView.seek(to: TimeInterval(sender.value))
        self.SeekBar.setValue(Float(sender.value), animated: true)
        self.videoVRView.play()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UIDevice.current.setValue(Int(UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
    
    }
    
    func canRotate() -> Void {}
    
    @IBAction func RestartBtnClicked(_ sender: UIButton) {
        
        self.loadVideo()
        
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        let ti = NSInteger(interval)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        //let hours = (ti / 3600)
        return NSString(format: "%0.2d:%0.2d",minutes,seconds)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    override var prefersStatusBarHidden: Bool {
        
        return true
    }

}
extension VideoPlayerController: GVRWidgetViewDelegate {
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {
        
    if content is NSURL {
                
        videoVRView.isHidden = false
        self.SeekBar.maximumValue = Float(videoVRView.duration())
        self.SeekBar.minimumValue = 0.0
        self.SeekBar.isContinuous = true
        videoVRView?.play()
        
        let totalTimeInMinutes = stringFromTimeInterval(interval: videoVRView.duration())
        
        self.TimeFrameLbl.text = "00:00" + "/" + (totalTimeInMinutes as String)
        
         
    }
        
    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChangeDisplayMode content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        currentView = widgetView
        currentDisplayMode = displayMode
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        
    }
  }
}

extension VideoPlayerController: GVRVideoViewDelegate {
    func videoView(_ videoView: GVRVideoView!, didUpdatePosition position: TimeInterval) {
        OperationQueue.main.addOperation() {
            if position >= videoView.duration() {
                videoView.seek(to: 0)
                videoView.play()
                
            }
   
            let currentTimeInMinutes = self.stringFromTimeInterval(interval: position)
            let totalTimeInMinutes = self.stringFromTimeInterval(interval: self.videoVRView.duration())
            
            self.SeekBar.setValue(Float(position), animated: true)
            self.TimeFrameLbl.text = "\(currentTimeInMinutes)" + "/" + (totalTimeInMinutes as String)
            
            if(totalTimeInMinutes ==  currentTimeInMinutes){
            
                self.videoVRView.pause()
                self.SeekBar.setValue(Float(position), animated: true)
                self.TimeFrameLbl.text = "\(currentTimeInMinutes)" + "/" + (totalTimeInMinutes as String)
                self.PlayBtn.isHidden = true
                self.RestartBtn.isHidden = false
                
            
            }
            
        }
    }

class TouchView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if let ViewController = viewController() as? ViewController , event?.type == UIEventType.touches {
            ViewController.setCurrentViewFromTouch(touchPoint: point)
        }
        return true
    }
    
    func viewController() -> UIViewController? {
        if self.next!.isKind(of: ViewController.self) {
            return self.next as? UIViewController
        } else {
            return nil
        }
    }
  }
}

