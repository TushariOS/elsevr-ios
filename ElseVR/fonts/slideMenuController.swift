//
//  slideMenuController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 23/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import SideMenu
class slideMenuController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var CrossBtn: UIButton!
    
    @IBOutlet weak var AboutText: UITextView!
    
    @IBOutlet weak var CultureText: UITextView!
    
    @IBOutlet weak var AnandText: UITextView!
    
    @IBOutlet weak var KhushbooText: UITextView!
 
    @IBOutlet weak var SubhangiText: UITextView!
    
    @IBOutlet weak var ZainText: UITextView!
    
    @IBOutlet weak var ShirinText: UITextView!
    
    @IBOutlet weak var ShoneText: UITextView!
    
    @IBOutlet weak var ContactText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    override func viewDidLayoutSubviews() {
        
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 2162)
        
        if(IS_IPHONE_5){
            
            scrollView.frame = CGRect(x: 0, y:88,width:self.view.frame.size.width,height: 438)
            
            AboutText.frame = CGRect(x: AboutText.frame.origin.x, y:AboutText.frame.origin.y,width:AboutText.frame.width,height: AboutText.frame.height)
            
            
            
        }else if(IS_IPHONE_6){
            
            scrollView.frame = CGRect(x: 0, y:88,width:self.view.frame.size.width,height: 515)
            
             AboutText.frame = CGRect(x: AboutText.frame.origin.x, y:AboutText.frame.origin.y,width:AboutText.frame.width - 50,height: AboutText.frame.height)
        
        }else if (IS_IPHONE_6Plus){
            
            scrollView.frame = CGRect(x: 0, y:88,width:self.view.frame.size.width,height: 568)
        
        }
        
    }
    @IBAction func crossBtnClicked(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
