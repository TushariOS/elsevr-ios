//
//  PlayVideoController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 07/03/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit

class PlayVideoController: UIViewController {
    
    @IBOutlet weak var fullScreenImage: UIImageView!
    @IBOutlet weak var PlayBtn: UIButton!
    @IBOutlet weak var PlayLbl: UILabel!
    
    @IBOutlet weak var WatchVRonHeadsetLbl: UILabel!
    @IBOutlet weak var WatchVRonHeadsetImg: UIButton!
    @IBOutlet weak var SeparatorView: UIView!
    @IBOutlet weak var WatchVRWithoutHeadsetLbl: UILabel!
    @IBOutlet weak var WatchVRWithoutHeadsetImg: UIButton!
    
    let defaults = UserDefaults.standard
    var ViewShouldRotate = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
         fullScreenImage.downloadImageFrom(link: CSVList[SELECTED_INDEX].backGroundImageURL, contentMode: UIViewContentMode.scaleAspectFill)

        
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        self.Settingframes()
        self.AnimatingStartingLabel()
        self.CreateBackBtn()
    }
  
    func Settingframes(){
        
        self.WatchVRonHeadsetLbl.isHidden =          true
        self.WatchVRonHeadsetImg.isHidden =          true
        self.SeparatorView.isHidden =                true
        self.WatchVRWithoutHeadsetImg.isHidden =     true
        self.WatchVRWithoutHeadsetLbl.isHidden =     true
   
        self.PlayBtn.frame =                         CGRect(x:130,y:207,width:60,height:60)
        self.PlayLbl.frame =                         CGRect(x:130,y:275,width:60,height:60)
        
        self.WatchVRonHeadsetLbl.frame =             CGRect(x:75,y:184,width:170,height:30)
        self.WatchVRonHeadsetImg.frame =             CGRect(x:130,y:222,width:60,height:35)
        self.SeparatorView.frame =                   CGRect(x:70,y:283,width:180,height:2)
        self.WatchVRWithoutHeadsetImg.frame =        CGRect(x:130,y:325,width:60,height:35)
        self.WatchVRWithoutHeadsetLbl.frame =        CGRect(x:75,y:368,width:170,height:30)
       
    }
    func CreateBackBtn(){
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named:"back_arrow.png"), for: UIControlState.normal)
        
        if(IS_IPHONE_5){
            
            backButton.frame = CGRect(x:290, y:15, width:20,height: 30)
            
        }else if(IS_IPHONE_6){
            
            backButton.frame = CGRect(x:345, y:15, width:20,height: 30)
            
        }else if (IS_IPHONE_6Plus){
            
            backButton.frame = CGRect(x:385, y:15, width:20,height: 30)
            
        }
        backButton.addTarget(self, action: #selector(DetailsViewController().backButtonPressed(sender:)), for:.touchUpInside)
        self.view.addSubview(backButton)
        
        
    }
    
    func backButtonPressed(sender: UIButton!) {
        
        let allVC = self.navigationController?.viewControllers
        for index  in 0..<allVC!.count {
            
            if  let ListVC = allVC![index] as? DetailsViewController {
                self.navigationController!.popToViewController(ListVC, animated: false)
            }
            
        }
        
    }
    

    
    func AnimatingStartingLabel(){
     
        self.PlayBtn.isHidden =     true
        self.PlayLbl.isHidden =     true
        
        UIView.animate(withDuration: 0.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.PlayBtn.alpha =    0.0
            self.PlayLbl.alpha =    0.0
            
        }, completion: {
            (finished: Bool) -> Void in
            self.PlayBtn.isHidden =     false
            self.PlayLbl.isHidden =     false
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.PlayBtn.alpha =    1.0
                self.PlayLbl.alpha =    1.0
            }, completion: nil)
        })
        
        
    }
    
    
    @IBAction func PlayButtonClicked(_ sender: UIButton) {
        
        self.PlayLbl.isHidden =                      true
        self.PlayBtn.isHidden =                      true
        self.WatchVRonHeadsetLbl.isHidden =          true
        self.WatchVRonHeadsetImg.isHidden =          true
        self.SeparatorView.isHidden =                true
        self.WatchVRWithoutHeadsetImg.isHidden =     true
        self.WatchVRWithoutHeadsetLbl.isHidden =     true
        
        self.WatchVRonHeadsetLbl.alpha =             0.0
        self.WatchVRonHeadsetImg.alpha =             0.0
        self.SeparatorView.alpha =                   0.0
        self.WatchVRWithoutHeadsetImg.alpha =        0.0
        self.WatchVRWithoutHeadsetLbl.alpha =        0.0
       
        // Fade out to set the text
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.SeparatorView.alpha =    1.0
            
        }, completion: {
            (finished: Bool) -> Void in
            
                self.WatchVRonHeadsetLbl.isHidden =          false
                self.WatchVRonHeadsetImg.isHidden =          false
                self.SeparatorView.isHidden =                false
                self.WatchVRWithoutHeadsetImg.isHidden =     false
                self.WatchVRWithoutHeadsetLbl.isHidden =     false
                
                self.WatchVRonHeadsetLbl.frame =             CGRect(x:75,y:283,width:170,height:30)
                self.WatchVRonHeadsetImg.frame =             CGRect(x:130,y:283,width:60,height:35)
                self.SeparatorView.frame =                   CGRect(x:70,y:283,width:180,height:2)
                self.WatchVRWithoutHeadsetImg.frame =        CGRect(x:130,y:283,width:60,height:35)
                self.WatchVRWithoutHeadsetLbl.frame =        CGRect(x:75,y:283,width:170,height:30)
            
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.WatchVRonHeadsetLbl.alpha =             1.0
                self.WatchVRonHeadsetImg.alpha =             1.0
                self.SeparatorView.alpha =                   1.0
                self.WatchVRWithoutHeadsetImg.alpha =        1.0
                self.WatchVRWithoutHeadsetLbl.alpha =        1.0
                
                self.WatchVRonHeadsetLbl.frame =             CGRect(x:75,y:184,width:170,height:30)
                self.WatchVRonHeadsetImg.frame =             CGRect(x:130,y:222,width:60,height:35)
                self.SeparatorView.frame =                   CGRect(x:70,y:283,width:180,height:2)
                self.WatchVRWithoutHeadsetImg.frame =        CGRect(x:130,y:325,width:60,height:35)
                self.WatchVRWithoutHeadsetLbl.frame =        CGRect(x:75,y:368,width:170,height:30)
                
            }, completion: nil)
        })
        
    }
    
    @IBAction func PlayVideoWithVRheadset(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ElseVRMain") as! ViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func PlayVideoWithoutVRheadset(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "VideoPlayer", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "VideoPlayerController") as! VideoPlayerController
        controller.backgroundImage = fullScreenImage.image 
        self.navigationController?.pushViewController(controller, animated: false)
    
    }
    
    override var prefersStatusBarHidden: Bool {
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
