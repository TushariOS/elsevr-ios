//
//  DownloadController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 02/03/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import MZDownloadManager


protocol DownloadViewControllerDelegate
{
    func changeDownloadingStatusResponse()
}



class DownloadController: UIViewController {
    
    
    @IBOutlet weak var fullScreenImage: UIImageView!
    @IBOutlet weak var DOWNLOADLbl: UILabel!
    @IBOutlet weak var DataChargesMayApplyLbl: UILabel!
    @IBOutlet weak var DownloadImageBtn: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var SeizethemomentLbl: UILabel!
    @IBOutlet weak var ReadStoryBtn: UIButton!
    @IBOutlet weak var CancelDownloadBtn: UIButton!
    @IBOutlet weak var ReadStoryLbl: UILabel!
    @IBOutlet weak var CancelDownloadLbl: UILabel!
    @IBOutlet weak var DownloadProgressImg: UIImageView!
    @IBOutlet weak var CancelYesBtn: UIButton!
    @IBOutlet weak var CancelNoBtn: UIButton!
    @IBOutlet weak var downloadhasbeencancelledLbl: UILabel!
    
    var downloadDelegate: DownloadViewControllerDelegate?
    
    var fileDownloadPath :String! = CSVList[SELECTED_INDEX].content +  CSVList[SELECTED_INDEX].id + ".mp4"
    
    //var fileDownloadPath :String! = "https://dl.dropbox.com/u/97700329/file1.mp4"
    
    let myDownloadPath = MZUtility.baseFilePath + "/My Videos"
    
    var downloadModel : [MZDownloadModel] = []
    
    var availableDownloadsArray: [String] = []
   
    let defaults = UserDefaults.standard
    
    var ProgressBarTimer: Timer!
    
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = "com.iosDevelopment.MZDownloadManager.BackgroundSession\(CSVList[SELECTED_INDEX].id)"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var completion = appDelegate.backgroundSessionCompletionHandler
        
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CreateBackBtn()
        self.SettingframesAndIsHidden()
        
        fullScreenImage.downloadImageFrom(link: CSVList[SELECTED_INDEX].backGroundImageURL, contentMode: UIViewContentMode.scaleAspectFill)
        
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        debugPrint("custom download path: \(myDownloadPath)")
        
        let allTasks = downloadManager.downloadingArray
        print(allTasks.count)
        
        
        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
            
            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
            
            print(SelectedTaskindex as! Int)
            if(downloadManager.downloadingArray.count > SelectedTaskindex as! Int - 1){
                
                self.downloadManager.resumeDownloadTaskAtIndex(SelectedTaskindex as! Int - 1)
                
                ProgressBarTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateProgessBar), userInfo: nil, repeats: true)
            }
            
            
                self.SetDownloadVideoAnimation()
            
        }else{
            
            self.AnimatingStartingLabel()
            
        }
    
    }
    
    func UpdateProgessBar(){
    
        self.progressBar?.progress = CURRENT_PROGESS_BAR
        if(CURRENT_PROGESS_BAR == 1.0){
            ProgressBarTimer.invalidate()
        
        }
    
    }
    
    func SettingframesAndIsHidden(){
        
        self.progressBar.isHidden =                 true
        self.SeizethemomentLbl.isHidden =           true
        self.ReadStoryBtn.isHidden =                true
        self.ReadStoryLbl.isHidden =                true
        self.CancelDownloadBtn.isHidden =           true
        self.CancelDownloadLbl.isHidden =           true
        self.DownloadProgressImg.isHidden =         true
        self.CancelYesBtn.isHidden =                true
        self.CancelNoBtn.isHidden =                 true
        self.downloadhasbeencancelledLbl.isHidden = true
    
        self.DownloadProgressImg.frame =            CGRect(x:145,y:160,width:30,height:35)
        self.progressBar.frame =                    CGRect(x:85,y:198,width:150,height:2)
        self.SeizethemomentLbl.frame =              CGRect(x:70,y:208,width:180,height:43)
        self.CancelYesBtn.frame =                   CGRect(x:131,y:272,width:44,height:44)
        self.CancelNoBtn.frame =                    CGRect(x:131,y:272,width:44,height:44)
        self.DownloadImageBtn.frame =               CGRect(x:144,y:198,width:32,height:42)
        self.DOWNLOADLbl.frame =                    CGRect(x:115,y:248,width:92,height:24)
        self.DataChargesMayApplyLbl.frame =         CGRect(x:63,y:315,width:194,height:38)
        self.downloadhasbeencancelledLbl.frame =    CGRect(x:70,y:265,width:180,height:40)
    
    }
     

    @IBAction func DownloadVideoBtnClicked(_ sender: UIButton) {
        
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        
        let Myfile = MZUtility.baseFilePath + "/My Videos/" + "\(CSVList[SELECTED_INDEX].id).mp4" 
        
        if !FileManager.default.fileExists(atPath: Myfile) {
            
            let fileURL  : NSString = fileDownloadPath as NSString
            var fileName : NSString = fileURL.lastPathComponent as NSString
            fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(fileName as String) as NSString)
            
             downloadManager.addDownloadTask(fileName as String, fileURL: fileURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, destinationPath: myDownloadPath)
            
            let allTasks = downloadManager.downloadingArray
            print(allTasks.count)
            
            let dict = ["\(CSVList[SELECTED_INDEX].id)": allTasks.count]
            defaults.set(dict, forKey: "\(CSVList[SELECTED_INDEX].id)")
            
        }
        
        
        self.SetDownloadVideoAnimation()
        
        
    }
    
    func SetDownloadVideoAnimation(){
        
        self.downloadhasbeencancelledLbl.isHidden = true
        self.DownloadImageBtn.isHidden =            true
        self.DOWNLOADLbl.isHidden =                 true
        self.DataChargesMayApplyLbl.isHidden =      true
        
        self.DownloadProgressImg.alpha = 0.0
        self.progressBar.alpha =         0.0
        self.SeizethemomentLbl.alpha =   0.0
        self.ReadStoryBtn.alpha =        0.0
        self.ReadStoryLbl.alpha =        0.0
        self.CancelDownloadBtn.alpha =   0.0
        self.CancelDownloadLbl.alpha =   0.0
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.DownloadProgressImg.alpha =    1.0
            self.progressBar.alpha =            1.0
            
        }, completion: {
            (finished: Bool) -> Void in
            
            self.SeizethemomentLbl.frame =  CGRect(x:70,y:178,width:180,height:43)
            self.ReadStoryBtn.frame =       CGRect(x:70,y:242,width:44,height:44)
            self.ReadStoryLbl.frame =       CGRect(x:65,y:294,width:55,height:35)
            self.CancelDownloadBtn.frame =  CGRect(x:206,y:242,width:44,height:44)
            self.CancelDownloadLbl.frame =  CGRect(x:201,y:294,width:55,height:35)
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.SeizethemomentLbl.alpha =  1.0
                self.ReadStoryBtn.alpha =       1.0
                self.ReadStoryLbl.alpha =       1.0
                self.CancelDownloadBtn.alpha =  1.0
                self.CancelDownloadLbl.alpha =  1.0
                
                self.SeizethemomentLbl.frame =  CGRect(x:70,y:208,width:180,height:43)
                self.ReadStoryBtn.frame =       CGRect(x:70,y:272,width:44,height:44)
                self.ReadStoryLbl.frame =       CGRect(x:65,y:324,width:55,height:35)
                self.CancelDownloadBtn.frame =  CGRect(x:206,y:272,width:44,height:44)
                self.CancelDownloadLbl.frame =  CGRect(x:201,y:324,width:55,height:35)
                
            }, completion: nil)
        })
        
        self.progressBar.isHidden =             false
        self.SeizethemomentLbl.isHidden =       false
        self.ReadStoryBtn.isHidden =            false
        self.ReadStoryLbl.isHidden =            false
        self.CancelDownloadBtn.isHidden =       false
        self.CancelDownloadLbl.isHidden =       false
        self.DownloadProgressImg.isHidden =     false
    
    }
    
    
    @IBAction func ReadStoryBtnClicked(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func CancelDownloadClicked(_ sender: UIButton) {
        
        self.ReadStoryBtn.frame =       CGRect(x:70,y:272,width:44,height:44)
        self.ReadStoryLbl.frame =       CGRect(x:65,y:324,width:55,height:35)
        self.CancelDownloadBtn.frame =  CGRect(x:206,y:272,width:44,height:44)
        self.CancelDownloadLbl.frame =  CGRect(x:201,y:324,width:55,height:35)
        
        self.CancelYesBtn.alpha =       0.0
        self.CancelNoBtn.alpha =        0.0
        
        // Fade out to set the text
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.SeizethemomentLbl.alpha =  0.0
            self.ReadStoryBtn.alpha =       0.0
            self.ReadStoryLbl.alpha =       0.0
            self.CancelDownloadBtn.alpha =  0.0
            self.CancelDownloadLbl.alpha =  0.0
            
            self.ReadStoryBtn.frame =       CGRect(x:140,y:272,width:44,height:44)
            self.ReadStoryLbl.frame =       CGRect(x:140,y:324,width:55,height:35)
            self.CancelDownloadBtn.frame =  CGRect(x:140,y:272,width:44,height:44)
            self.CancelDownloadLbl.frame =  CGRect(x:140,y:324,width:55,height:35)
           
        }, completion: {
            (finished: Bool) -> Void in
            
            //Once the label is completely invisible, set the text and fade it back in
            self.SeizethemomentLbl.text = "Are you sure you want to cancel your download?"
            
            self.ReadStoryBtn.isHidden =        true
            self.ReadStoryLbl.isHidden =        true
            self.CancelDownloadBtn.isHidden =   true
            self.CancelDownloadLbl.isHidden =   true
            self.CancelYesBtn.isHidden =        false
            self.CancelNoBtn.isHidden =         false
            
            
            self.CancelYesBtn.frame =           CGRect(x:140,y:272,width:44,height:44)
            self.CancelNoBtn.frame =            CGRect(x:140,y:272,width:44,height:44)
            
            self.CancelYesBtn.alpha =           0.0
            self.CancelNoBtn.alpha =            0.0
            
            // Fade in
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                
                self.CancelYesBtn.frame =       CGRect(x:70,y:272,width:44,height:44)
                self.CancelNoBtn.frame =        CGRect(x:206,y:272,width:44,height:44)
                
                self.SeizethemomentLbl.alpha =  1.0
                self.CancelYesBtn.alpha =       1.0
                self.CancelNoBtn.alpha =        1.0
                
            }, completion: nil)
        })
        
        
        
    }
    
    @IBAction func CancelDownloadYesClicked(_ sender: UIButton) {
        
        let allTasks = downloadManager.downloadingArray
        print(allTasks.count)
      
        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
        
            let index = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
            self.downloadManager.cancelTaskAtIndex(Int(index as! Int) - 1)
            
            defaults.set(nil, forKey: "\(CSVList[SELECTED_INDEX].id)")
            self.downloadManager.urlSessionDidFinishEvents(forBackgroundURLSession: URLSession.init())
        
        }

        
        self.progressBar.isHidden =                 true
        self.SeizethemomentLbl.isHidden =           true
        self.ReadStoryBtn.isHidden =                true
        self.ReadStoryLbl.isHidden =                true
        self.CancelDownloadBtn.isHidden =           true
        self.CancelDownloadLbl.isHidden =           false
        self.DownloadProgressImg.isHidden =         true
        self.CancelYesBtn.isHidden =                true
        self.CancelNoBtn.isHidden =                 true
        self.downloadhasbeencancelledLbl.isHidden = false
        self.DownloadImageBtn.isHidden =            false
        
    }
    
    @IBAction func CancelDownloadNoClicked(_ sender: UIButton) {
        

        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.SeizethemomentLbl.alpha =  0.0
            self.ReadStoryBtn.alpha =       0.0
            self.ReadStoryLbl.alpha =       0.0
            self.CancelDownloadBtn.alpha =  0.0
            self.CancelDownloadLbl.alpha =  0.0
            
            self.CancelYesBtn.alpha =       0.0
            self.CancelNoBtn.alpha =        0.0
            
            self.ReadStoryBtn.frame =       CGRect(x:140,y:272,width:44,height:44)
            self.ReadStoryLbl.frame =       CGRect(x:140,y:324,width:55,height:35)
            self.CancelDownloadBtn.frame =  CGRect(x:140,y:272,width:44,height:44)
            self.CancelDownloadLbl.frame =  CGRect(x:140,y:324,width:55,height:35)
            self.CancelYesBtn.frame =       CGRect(x:140,y:272,width:44,height:44)
            self.CancelNoBtn.frame =        CGRect(x:140,y:272,width:44,height:44)
           
            
            
            
        }, completion: {
            (finished: Bool) -> Void in
            
            //Once the label is completely invisible, set the text and fade it back in
            self.SeizethemomentLbl.text = " Seize the moment.Read the  story while the film downloads. "
            
            self.ReadStoryBtn.isHidden =        false
            self.ReadStoryLbl.isHidden =        false
            self.CancelDownloadBtn.isHidden =   false
            self.CancelDownloadLbl.isHidden =   false
            self.CancelYesBtn.isHidden =        true
            self.CancelNoBtn.isHidden =         true
            
            
           
            
            self.CancelYesBtn.alpha =       0.0
            self.CancelNoBtn.alpha =        0.0
            
            
            // Fade in
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.SeizethemomentLbl.alpha = 1.0
                
                self.CancelYesBtn.frame =       CGRect(x:70,y:272,width:44,height:44)
                self.CancelNoBtn.frame =        CGRect(x:206,y:272,width:44,height:44)
                self.ReadStoryBtn.frame =       CGRect(x:70,y:272,width:44,height:44)
                self.ReadStoryLbl.frame =       CGRect(x:65,y:324,width:55,height:35)
                self.CancelDownloadBtn.frame =  CGRect(x:206,y:272,width:44,height:44)
                self.CancelDownloadLbl.frame =  CGRect(x:201,y:324,width:55,height:35)
                
                self.ReadStoryBtn.alpha =       1.0
                self.ReadStoryLbl.alpha =       1.0
                self.CancelDownloadBtn.alpha =  1.0
                self.CancelDownloadLbl.alpha =  1.0
                
                self.CancelYesBtn.alpha =       1.0
                self.CancelNoBtn.alpha =        1.0
                
            }, completion: nil)
        })
       
    }
    
  
    func backButtonPressed(sender: UIButton!) {
        
        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
            
            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
            
            print(SelectedTaskindex as! Int)
            if(downloadManager.downloadingArray.count > SelectedTaskindex as! Int - 1){
                
                if (ProgressBarTimer != nil){
                
                    ProgressBarTimer.invalidate()
                    
                }
                
            }
            
        }
        
       self.downloadDelegate?.changeDownloadingStatusResponse()
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func CreateBackBtn(){
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named:"back_arrow.png"), for: UIControlState.normal)
        
        if(IS_IPHONE_5){
            
            backButton.frame = CGRect(x:290, y:15, width:20,height: 30)
            
        }else if(IS_IPHONE_6){
            
            backButton.frame = CGRect(x:345, y:15, width:20,height: 30)
            
        }else if (IS_IPHONE_6Plus){
            
            backButton.frame = CGRect(x:385, y:15, width:20,height: 30)
            
        }
        backButton.addTarget(self, action: #selector(DetailsViewController().backButtonPressed(sender:)), for:.touchUpInside)
        self.view.addSubview(backButton)
        
        
    }
    func AnimatingStartingLabel(){
        
        self.DownloadImageBtn.isHidden =        true
        self.DOWNLOADLbl.isHidden =             true
        self.DataChargesMayApplyLbl.isHidden =  true

        
        UIView.animate(withDuration: 0.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.DownloadImageBtn.alpha =       0.0
            self.DOWNLOADLbl.alpha =            0.0
            self.DataChargesMayApplyLbl.alpha = 0.0
            
        }, completion: {
            (finished: Bool) -> Void in
            
            self.DownloadImageBtn.isHidden =        false
            self.DOWNLOADLbl.isHidden =             false
            self.DataChargesMayApplyLbl.isHidden =  false

            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.DownloadImageBtn.alpha =       1.0
                self.DOWNLOADLbl.alpha =            1.0
                self.DataChargesMayApplyLbl.alpha = 1.0
            }, completion: nil)
        })
        
        
    }
 
}
extension DownloadController: MZDownloadManagerDelegate {
    
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        
//        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
//            
//            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
//              DispatchQueue.main.async {
//                
//                self.progressBar?.progress = self.downloadManager.downloadingArray[Int(SelectedTaskindex as! Int) - 1].progress
//                
//            }
//            
//        }
        
    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
        

        
    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
        
      DispatchQueue.global(qos: .userInteractive).async {
        
        if  let SelectedTask = self.defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
            
            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"] as! Int - 1
            
            if(self.downloadManager.downloadingArray.count > SelectedTaskindex){
                DispatchQueue.main.async {
                    
                    if("\(CSVList[SELECTED_INDEX].id).mp4" == self.downloadManager.downloadingArray[Int(SelectedTaskindex)].fileName){
                    
                    self.progressBar?.progress = self.downloadManager.downloadingArray[Int(SelectedTaskindex)].progress
                    CURRENT_PROGESS_BAR = self.downloadManager.downloadingArray[Int(SelectedTaskindex)].progress
                    print(CURRENT_PROGESS_BAR)
                    print(self.downloadManager.downloadingArray[Int(SelectedTaskindex)].fileName)
                        
                    }
               }
            }
         }
      }
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
        
//        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
//            
//            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
//              DispatchQueue.main.async {
//            self.progressBar?.progress = self.downloadManager.downloadingArray[Int(SelectedTaskindex as! Int) - 1].progress
//                
//            }
//            
//        }
        
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
        
//        if  let SelectedTask = defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary{
//            
//            let SelectedTaskindex = SelectedTask["\(CSVList[SELECTED_INDEX].id)"]
//              DispatchQueue.main.async {
//            self.progressBar?.progress =   self.downloadManager.downloadingArray[Int(SelectedTaskindex as! Int) - 1].progress
//                
//            }
//            
//        }
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        
        DispatchQueue.main.async {
            self.progressBar?.progress =   0
            self.defaults.set(nil, forKey: "\(CSVList[SELECTED_INDEX].id)")
            CURRENT_PROGESS_BAR = 0
            
        }

    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
        
        defaults.set(nil, forKey: "\(CSVList[SELECTED_INDEX].id)")
        print(CSVList[SELECTED_INDEX].id)
        let docDirectoryPath : NSString = (MZUtility.baseFilePath + "/My Videos" as NSString).appendingPathComponent(downloadModel.fileName) as NSString
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: MZUtility.DownloadCompletedNotif as String), object: docDirectoryPath)
        
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let PlayVideoController = storyboard.instantiateViewController(withIdentifier: "PlayVideo") as! PlayVideoController
            self.navigationController?.pushViewController(PlayVideoController, animated: false)
        }
       
       
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        
        debugPrint("Error while downloading file: \(downloadModel.fileName)  Error: \(error)")
        defaults.set(nil, forKey: "\(CSVList[SELECTED_INDEX].id)")
    }
    
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        
        let myDownloadPath = MZUtility.baseFilePath + "/My Videos"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
}
