//
//  RotateToLandscapeViewClass.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 10/03/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import Foundation


class NavigationController: UINavigationController {
    
    override var shouldAutorotate: Bool {
        return true
    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .landscapeRight
    }
}
