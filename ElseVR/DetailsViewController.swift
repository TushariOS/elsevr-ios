//
//  DetailsViewController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 23/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//



import UIKit
import SwiftHTTP
import Toaster
import MZDownloadManager

class DetailsViewController: UIViewController, UIViewControllerTransitioningDelegate ,UIWebViewDelegate,UIGestureRecognizerDelegate, UIScrollViewDelegate ,DownloadViewControllerDelegate{
    
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet var webVideoText: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var playLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var progressBar2: UIProgressView!
    
    var herb: UIImage!
    var array: [Int] = []
    var fittingSize : CGSize = CGSize.init(width: 320, height: 568)
    var SHOULD_SCROLL = false
    let defaults = UserDefaults.standard
    let myDownloadPath = MZUtility.baseFilePath + "/My Videos"
    var ProgressBarTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AnimatingStartingLabel()
        self.CreateBackBtn()
        self.LoadHTMLPage()
        self.navigationController?.isNavigationBarHidden = true
        
        
        if(IS_IPHONE_5){
            
            fittingSize = CGSize.init(width: 320, height: 568)
            
        }else if(IS_IPHONE_6){
            
           fittingSize = CGSize.init(width: 375, height: 667)
            
            
        }else if (IS_IPHONE_6Plus){
            
            fittingSize = CGSize.init(width: 414, height: 736)
            
            
        }
        if  (defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary) != nil{
            
            self.playLabel.text =  "DOWNLOADING..."
             ProgressBarTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateProgessBar), userInfo: nil, repeats: true)
            
            
        }
        
        let localPath = myDownloadPath.appending("/\(CSVList[SELECTED_INDEX].id).mp4")
        
        if FileManager.default.fileExists(atPath: localPath) {
            
            self.playLabel.text =  "\(CSVList[SELECTED_INDEX].textTitle)"
            
        }
        
        webVideoText.scrollView.showsVerticalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        webVideoText.delegate = self
        webVideoText.scrollView.isScrollEnabled = false
        scrollView.isScrollEnabled = true
        scrollView.delegate = self
        webVideoText.scrollView.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(DetailsViewController.AddScrollGesture(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.delegate = self
        self.webVideoText.scrollView.addGestureRecognizer(gesture)
        
//        self.view.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height)
//        self.playButton.frame = CGRect(x:140,y:90,width:40,height:40)
//        self.playLabel.frame = CGRect(x:85,y:135,width:150,height:25)
//        bgImage.frame = CGRect(x:0,y:0,width:320,height:200)
//        progressBar.frame = CGRect(x:0,y:201,width:320,height:2)
        
        bgImage.image = herb
        bgImage.contentMode = .scaleAspectFill
        bgImage.downloadImageFrom(link: CSVList[SELECTED_INDEX].fullImage, contentMode: UIViewContentMode.scaleAspectFill)
    
    }
    
    func UpdateProgessBar(){
        
        self.progressBar?.progress = CURRENT_PROGESS_BAR
        self.progressBar2?.progress = CURRENT_PROGESS_BAR
        
        if(CURRENT_PROGESS_BAR == 1.0){
            ProgressBarTimer.invalidate()
        }
        
    }
    
    func changeDownloadingStatusResponse(){
        
        if  (defaults.value(forKey: "\(CSVList[SELECTED_INDEX].id)") as? NSDictionary) != nil{
            
            self.playLabel.text =  "DOWNLOADING..."
            
        }else{
        
            self.playLabel.text = CSVList[SELECTED_INDEX].textTitle
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        return false
        
    }

    func gestureRecognizer(_: UIGestureRecognizer,  shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    
    @IBAction func PlayBtnClicked(_ sender: UIButton) {
        
        let localPath = myDownloadPath.appending("/\(CSVList[SELECTED_INDEX].id).mp4")
        
        if FileManager.default.fileExists(atPath: localPath) {
            
            let PlayVideoController = storyboard!.instantiateViewController(withIdentifier: "PlayVideo") as! PlayVideoController
            self.navigationController?.pushViewController(PlayVideoController, animated: false)
            
        }else{
            
            let DownloadController = storyboard!.instantiateViewController(withIdentifier: "DownloadController") as! DownloadController
            DownloadController.downloadDelegate = self
            self.navigationController?.pushViewController(DownloadController, animated: false)
            
        }
        
        
    }
    
    func AddScrollGesture(_ sender:UITapGestureRecognizer){
        
        if(SHOULD_SCROLL == false){
            SHOULD_SCROLL = true
            self.webVideoText.scrollView.isScrollEnabled = true
        
        }else{
        
            SHOULD_SCROLL = false
            self.webVideoText.scrollView.isScrollEnabled = false
        }
    
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       // if(scrollView == webVideoText){
        print("scrolled")
       // }
    }
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        print("top scrolled")
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("top scrolledDragging")
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        print("top scrolledDragging")
    }
 
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        var frame : CGRect = webVideoText.frame
        frame.size.height = 1
        webVideoText.frame = frame
        fittingSize = webVideoText.sizeThatFits(CGSize.zero)
        frame.size = fittingSize
        webVideoText.frame = frame
        
        
        print("\(fittingSize.width) + \(fittingSize.height)")
        
        webVideoText.frame = CGRect(x:0,y:203,width:self.view.frame.width,height:fittingSize.height)
        webVideoText.window?.setNeedsUpdateConstraints()
        webVideoText.window?.setNeedsLayout()
        self.viewDidLayoutSubviews()
    
    }

    override func viewDidLayoutSubviews() {
        
        if(IS_IPHONE_5){
            
            scrollView.isScrollEnabled = true
            scrollView.frame = CGRect(x:0,y:0,width:320,height:568)
            scrollView.contentSize = CGSize(width:320,height: fittingSize.height + 203)
            print("scroll view height is \(fittingSize.height + 203)")
            
        }else if(IS_IPHONE_6){
            
            scrollView.isScrollEnabled = true
            scrollView.frame = CGRect(x:0,y:0,width:375,height:667)
            scrollView.contentSize = CGSize(width:375,height: fittingSize.height + 203)
            print("scroll view height is \(fittingSize.height + 203)")
            
            
        }else if (IS_IPHONE_6Plus){
            
            scrollView.isScrollEnabled = true
            scrollView.frame = CGRect(x:0,y:0,width:414,height:736)
            scrollView.contentSize = CGSize(width:414,height: fittingSize.height + 203)
            print("scroll view height is \(fittingSize.height + 203)")
            
            
        }
        
       
    }
    
    func saveHtmlPage(){
        
        do {
            let url =  API_HTML_TEXT + CSVList[SELECTED_INDEX].id + ".html"
            let opt = try HTTP.GET(url)
            opt.start { response in
                if let err = response.error {
                    print("error: \(err.localizedDescription)")
                    Toast(text: "file not found", duration: Delay.long).show()
                    return //also notify app of failure as needed
                }
                
                if let data: String = response.text {
                   
                    let file = "\(CSVList[SELECTED_INDEX].id).html" //this is the file. we will write to and read from it
                    
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        
                        let path = dir.appendingPathComponent(
                            file)
                        
                        //writing
                        do {
                            try data.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                            
                            
                        }
                        catch {/* error handling here */}
                        
                        
                        let fileUrl = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                        let url = NSURL(fileURLWithPath: fileUrl)
                        let filePath = url.appendingPathComponent("\(CSVList[SELECTED_INDEX].id).html" )?.path
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: filePath!) {
                            self.webVideoText.loadRequest(URLRequest(url:path))
                        }else{
                            
                            Toast(text: "file not found", duration: Delay.long).show()
                            
                        }
                        
                    }
                    
                }
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }

  
    func backButtonPressed(sender: UIButton!) {
        
     _ = self.navigationController?.popViewController(animated: false)
       
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func CreateBackBtn(){
        
        let backButton = UIButton()
        backButton.setImage(UIImage(named:"back_arrow.png"), for: UIControlState.normal)
       
        if(IS_IPHONE_5){
            
            backButton.frame = CGRect(x:290, y:15, width:20,height: 30)
            
        }else if(IS_IPHONE_6){
            
            backButton.frame = CGRect(x:345, y:15, width:20,height: 30)
            
        }else if (IS_IPHONE_6Plus){
            
            backButton.frame = CGRect(x:385, y:15, width:20,height: 30)
            
        }
        backButton.addTarget(self, action: #selector(DetailsViewController().backButtonPressed(sender:)), for:.touchUpInside)
        self.view.addSubview(backButton)
        
        
    }
    func LoadHTMLPage(){
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent("\(CSVList[SELECTED_INDEX].id).html" )?.path
        
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        let htmlPath = dir?.appendingPathComponent("\(CSVList[SELECTED_INDEX].id).html")
        let fileManager = FileManager.default
        
        if isInternetAvailable(){
            
            self.saveHtmlPage()
            
        }else if fileManager.fileExists(atPath: filePath!) {
            
            self.webVideoText.loadRequest(URLRequest(url:htmlPath!))
            
        }else{
            
            Toast(text: NO_INTERNET_MSG, duration: Delay.long).show()
            
        }
        
        
    }
    
    func AnimatingStartingLabel(){
        
        self.playLabel.text =           CSVList[SELECTED_INDEX].textTitle
        self.playButton.isHidden =      true
        self.playLabel.isHidden =       true

        
        UIView.animate(withDuration: 0.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.playButton.alpha =         0.0
            self.playLabel.alpha =          0.0
            
        }, completion: {
            (finished: Bool) -> Void in
            self.playButton.isHidden = false
            self.playLabel.isHidden = false
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.playButton.alpha =     1.0
                self.playLabel.alpha =      1.0
            }, completion: nil)
        })
        
        
    }
   
    
}
