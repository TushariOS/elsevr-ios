//
//  VideoListingController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 23/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import SideMenu
class VideoListingController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var AmemesysRealityProjectLbl: UILabel!
    @IBOutlet var memesysLbl: UILabel!
    @IBOutlet var topBlackView: UIView!
    
    var selectedImage: UIImageView?
    let transition = PopAnimator()
    var tableData:[AnyObject]!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topBlackView.isHidden = true
        self.tableData = []
        
        let menuLeftNavigationController = UISideMenuNavigationController()
        menuLeftNavigationController.leftSide = true
        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuWidth = max(round(min((self.view.bounds.width), (self.view.bounds.height)) * 0.90), 240)
        
        self.tableView.contentInset = UIEdgeInsets(top: 99,left: 0,bottom: 0,right: 0)
        self.navigationController?.isNavigationBarHidden = true
        
        let memesyProjectsString = NSMutableAttributedString(string: "A MEMESYS REALITY PROJECT")
        memesyProjectsString.addAttribute(NSKernAttributeName, value: CGFloat(1.0), range: NSRange(location: 0, length: memesyProjectsString.length))
        AmemesysRealityProjectLbl.attributedText = memesyProjectsString
        self.AmemesysRealityProjectLbl.adjustsFontSizeToFitWidth = true
        
        let memsysString = NSMutableAttributedString(string: "MEMESYS")
        memsysString.addAttribute(NSKernAttributeName, value: CGFloat(3.0), range: NSRange(location: 0, length: memsysString.length))
        memesysLbl.attributedText = memsysString
        self.memesysLbl.adjustsFontSizeToFitWidth = true

        // Do any additional setup after loading the view.
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CSVList.count
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.topBlackView.isHidden = false
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "VideoListCell"
        let cell :videoListingCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! videoListingCell?
            ?? UITableViewCell(style: .default, reuseIdentifier: cellIdentifier) as! videoListingCell
        
        cell.VideoImageView.tag = indexPath.row
        cell.VideoImageView.layoutSubviews()
        cell.VideoImageView.image = UIImage(named: "")
        if(IS_IPHONE_6){
            
            cell.VideoImageView!.frame = CGRect(x: 15, y:10,width:345,height: 192)
            
        }
        else if (IS_IPHONE_6Plus){
            
            cell.VideoImageView!.frame = CGRect(x: 20, y:10,width:375,height: 202)
            
        }
        
        if (cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) != nil){
       
            print("Cached image used, no need to download it")
            DispatchQueue.main.async {
            cell.VideoImageView.image = cache.object(forKey: (indexPath as NSIndexPath).row as AnyObject) as? UIImage
            cell.VideoImageView.layoutSubviews()
            }
            
        }else{
            // 3
            let artworkUrl = CSVList[indexPath.row].author
            let url:URL! = URL(string: artworkUrl)
            task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: url){
                    // 4
                    DispatchQueue.main.async(execute: { () -> Void in
                        // 5
                        // Before we assign the image, check whether the current cell is visible
                        if let updateCell = tableView.cellForRow(at: indexPath) as? videoListingCell  {
                            let img:UIImage! = UIImage(data: data)
                            updateCell.VideoImageView.image = img
                            updateCell.VideoImageView.layoutSubviews()
                            cache.setObject(img, forKey: (indexPath as NSIndexPath).row as AnyObject)
                        }
                    })
                }
            })
            task.resume()
        
        }
        return cell

    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(IS_IPHONE_6Plus){
        
            return 220
            
        }else if(IS_IPHONE_6){
                
            return 210
                
        }else{
            
            return 180
        
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as? videoListingCell {
            
            selectedImage = cell.VideoImageView
            let index = cell.VideoImageView.tag
            SELECTED_INDEX = index
            
            let Details = storyboard!.instantiateViewController(withIdentifier: "DetailsView") as! DetailsViewController
            Details.herb = cache.object(forKey: index as AnyObject) as? UIImage
           // Details.transitioningDelegate = self
           // present(Details, animated: true, completion: nil)
            self.navigationController?.pushViewController(Details, animated: false)
             }
        
    }
  
    override var prefersStatusBarHidden: Bool {
        
        return true
        
    }
    
    @IBAction func AddPanGesture(_ sender: Any) {
        
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.tableView)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.topBlackView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
//Download Image
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        
        let trimmedString = link.replacingOccurrences(of: " ", with: "")
        
        URLSession.shared.dataTask( with: NSURL(string:trimmedString)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async() {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

//extension VideoListingController: UIViewControllerTransitioningDelegate {
//    func animationController(
//        forPresented presented: UIViewController,
//        presenting: UIViewController,
//        source: UIViewController) ->
//        UIViewControllerAnimatedTransitioning? {
//            
//            transition.originFrame = selectedImage!.superview!.convert(selectedImage!.frame, to: nil)
//            transition.presenting = true
//            
//            return transition
//    }
//    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.presenting = false
//        return transition
//    }
//}


