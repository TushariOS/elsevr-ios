//
//  HomeViewController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 22/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import SwiftHTTP
import CSV
import Toaster

let ButtonWidth  : CGFloat = 230.0
let ButtonHeight : CGFloat = 40.0
let MarginY      : CGFloat = 10.0
let MarginX      = (UIScreen.main.bounds.size.width - ButtonWidth) / 2
let ThemeColor   = UIColor.darkGray

class HomeViewController: UIViewController {
    
    @IBOutlet weak var slideImageView: GVRPanoramaView!
    @IBOutlet weak var ElseVRGif: UIImageView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var enterThestoryLbl: UILabel!
    
    enum Media {
        static var photoArray = ["image1.jpg", "image2.jpg","image3.jpg","image4.jpg","image5.jpg","image6.jpg"]
        static var gifImageArray = ["frame_0_delay-0.07s.gif","frame_1_delay-0.07s.gif","frame_2_delay-0.07s.gif","frame_3_delay-0.07s.gif","frame_4_delay-0.07s.gif","frame_5_delay-0.07s.gif","frame_6_delay-0.07s.gif","frame_7_delay-0.07s.gif","frame_8_delay-0.07s.gif","frame_9_delay-0.07s.gif","frame_10_delay-0.07s.gif","frame_11_delay-0.07s.gif","frame_12_delay-0.07s.gif","frame_13_delay-0.07s.gif","frame_14_delay-0.07s.gif","frame_15_delay-0.07s.gif","frame_16_delay-0.07s.gif","frame_17_delay-0.07s.gif","frame_18_delay-0.07s.gif","frame_19_delay-0.07s.gif","frame_20_delay-0.07s.gif","frame_21_delay-0.07s.gif","frame_22_delay-0.07s.gif","frame_23_delay-0.07s.gif","frame_24_delay-0.07s.gif","frame_25_delay-0.07s.gif","frame_26_delay-0.07s.gif","frame_27_delay-0.07s.gif"]
    }
    
    var currentView: UIView?
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    var imageTimer: Timer!
    var gifTimer: Timer!
    var imgListArray : [UIImage] = []
    var csvData : String = ""
    var presentWindow : UIWindow?
    var ShouldNavigate: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        cache = NSCache()
        session = URLSession.shared
        task = URLSessionDownloadTask()
        
        self.navigationController?.isNavigationBarHidden = true
        
        let storyAttributedString = NSMutableAttributedString(string: "ENTER THE STORY")
        storyAttributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.0), range: NSRange(location: 0, length: storyAttributedString.length))
        enterThestoryLbl.attributedText = storyAttributedString
        self.enterThestoryLbl.adjustsFontSizeToFitWidth = true
        
        //set enterThestoryLbl font size
        if(IS_IPHONE_6){
        
            
        }else if(IS_IPHONE_6Plus){
         
        
        }
        
        self.slideImageView.load(UIImage(named: Media.photoArray.first!),of: GVRPanoramaImageType.mono)
        self.slideImageView.enableCardboardButton = false
        self.slideImageView.enableFullscreenButton = false
        self.slideImageView.enableInfoButton = false
        self.slideImageView.delegate = self
        self.slideImageView.isHidden = true
        
        imageTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(changeImage), userInfo: nil, repeats: true)
        
        gifTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(stopGif), userInfo: nil, repeats: false)
        
        
        for countValue in Media.gifImageArray
        {
            let image  = UIImage(named:countValue)
            imgListArray.append(image!)
        }
        
        self.ElseVRGif.animationImages = imgListArray
        self.ElseVRGif.animationDuration = 2
        self.ElseVRGif.startAnimating()
        
        if isInternetAvailable()  == true{
        
            self.getCSVfile()
            
        }else{
            
            Toast(text: NO_INTERNET_MSG, duration: Delay.long).show()
            
        }
        
        
    }

    
    @IBAction func startBtnClicked(_ sender: UIButton) {
        
        if isInternetAvailable() == true{
            
            if(CSVList.count>0){

                 self.performSegue(withIdentifier: "segueToListing", sender: self)
                
            }else{
                
                ShouldNavigate = true
                Toast(text: NO_INTERNET_MSG, duration: Delay.long).show()
                self.getCSVfile()
            
            }
        }
        else{
            
            Toast(text: NO_INTERNET_MSG, duration: Delay.long).show()
        
        }
        
        
    }

    
    func changeImage() {
        
        Media.photoArray.append(Media.photoArray.removeFirst())
        DispatchQueue.main.async(){
        self.slideImageView?.load(UIImage(named: Media.photoArray.first!), of: GVRPanoramaImageType.mono)
        self.slideImageView.layoutIfNeeded()
        }
    }
    
    
    func stopGif() {
        self.ElseVRGif.stopAnimating()
        self.ElseVRGif.image = UIImage(named:"frame_27_delay-0.07s.gif")
        
    }
    
    func getCSVfile(){
        
        do {
            let url = API_CSV_FILE4K
            let opt = try HTTP.GET(url)
            opt.start { response in
                if let err = response.error {
                    print("error: \(err.localizedDescription)")
                    Toast(text: "file not found", duration: Delay.long).show()
                    return //also notify app of failure as needed
                }
                
                if let data: String = response.text {
                    
                    let NSdata = try! CSV(
                        string: data,
                        hasHeaderRow: false) // default: false
                    
                    if(CSVList.count == 0){
                        for row in NSdata {
                            let singleCSVModel = CSVModel(jsonNSArray: row as NSArray)
                            CSVList.append(singleCSVModel)
                            
                            
                        }
                    }
                    
                    for i in 0...CSVList.count - 1{
                        let artworkUrl = CSVList[i].author
                        let url:URL! = URL(string: artworkUrl)
                        task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                            if let data = try? Data(contentsOf: url){
                                // 4
                                DispatchQueue.main.async(execute: { () -> Void in
                                    let img:UIImage! = UIImage(data: data)
                                    cache.setObject(img, forKey: i as AnyObject)
                                    
                                })
                            }
                        })
                        task.resume()
                    
                    }
                    
                    if(self.ShouldNavigate == true){

                        self.performSegue(withIdentifier: "segueToListing", sender: self)
                        
                    }
                    
                    
                }
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
        
    }
    
 
    override func viewDidDisappear(_ animated: Bool) {
        imageTimer.invalidate()
        gifTimer.invalidate()
    }
    
    class TouchView: UIView {
        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            if let ViewController = viewController() as? ViewController , event?.type == UIEventType.touches {
                ViewController.setCurrentViewFromTouch(touchPoint: point)
            }
            return true
        }
        
        func viewController() -> UIViewController? {
            if self.next!.isKind(of: ViewController.self) {
                return self.next as? UIViewController
            } else {
                return nil
            }
        }
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        ToastCenter.default.cancelAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension HomeViewController: GVRWidgetViewDelegate {
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {
        if content is UIImage {
            self.slideImageView.isHidden = false
            
        } else
            if content is NSURL {
                
        }
        
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        currentView = widgetView
        currentDisplayMode = displayMode
       
        if currentView == slideImageView && currentDisplayMode != GVRWidgetDisplayMode.embedded {
            view.isHidden = true
        } else {
            view.isHidden = false
        }
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        guard currentDisplayMode != GVRWidgetDisplayMode.embedded else {return}
      
    }
}
