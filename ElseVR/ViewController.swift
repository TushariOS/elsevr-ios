//
//  ViewController.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 20/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SceneKit


class ViewController: UIViewController {
   
    @IBOutlet var videoVRView: GVRVideoView!
    
    var currentView: UIView?
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    
    override func viewDidLoad() {
        super.viewDidLoad()

        videoVRView.isHidden = true
        
       if  let path =  Bundle.main.path(forResource:"6", ofType:"mp4") {
        
            let fileUrl = URL(fileURLWithPath: path)
            self.videoVRView.load(from: fileUrl, of: GVRVideoType.mono)
            self.videoVRView.enableCardboardButton = false
            self.videoVRView.enableFullscreenButton = false
            self.videoVRView.enableInfoButton = false
            self.videoVRView.displayMode = .fullscreenVR
            self.videoVRView.delegate = self
        }

    }
    
    
    func refreshVideoPlayStatus() {
        if currentView == videoVRView && currentDisplayMode != GVRWidgetDisplayMode.embedded {
            videoVRView?.play()
        } else {
            videoVRView?.pause()
        }
    }
    
    func setCurrentViewFromTouch(touchPoint point:CGPoint) {
        if videoVRView!.frame.contains(point) {
            currentView = videoVRView
        }
    }
}

extension ViewController: GVRWidgetViewDelegate {
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {

            if content is NSURL {
                videoVRView.isHidden = false
                refreshVideoPlayStatus()
        }
        
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        currentView = widgetView
        currentDisplayMode = displayMode
        refreshVideoPlayStatus()
        if  currentDisplayMode != GVRWidgetDisplayMode.embedded {
            view.isHidden = true
        } else {
            view.isHidden = false
            let allVC = self.navigationController?.viewControllers
            for index  in 0..<allVC!.count {
                
                if  let ListVC = allVC![index] as? DetailsViewController {
                    self.navigationController!.popToViewController(ListVC, animated: false)
                }
                
            }
        }
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        guard currentDisplayMode != GVRWidgetDisplayMode.embedded else {return}
    
    }
}

extension ViewController: GVRVideoViewDelegate {
    func videoView(_ videoView: GVRVideoView!, didUpdatePosition position: TimeInterval) {
        OperationQueue.main.addOperation() {
            if position >= videoView.duration() {
                videoView.seek(to: 0)
                videoView.play()
            }
        }
    }
}

class TouchView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if let ViewController = viewController() as? ViewController , event?.type == UIEventType.touches {
            ViewController.setCurrentViewFromTouch(touchPoint: point)
        }
        return true
    }
    
    func viewController() -> UIViewController? {
        if self.next!.isKind(of: ViewController.self) {
            return self.next as? UIViewController
        } else {
            return nil
        }
    }
}
