//
//  Utils.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 21/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Utils {
    static let defaults = UserDefaults.standard
    
    
    static var counterGuestSyncResponsesReceived = 0
    static var countOfSyncGuestRequestsSent = 0
    
    static func saveDeviceToken(password: String) {
        defaults.setValue(password, forKey: "DEVICE_TOKEN")
        defaults.synchronize()
        
    }
    
    static func getDeviceToken()-> String {
        if let devicePassword = defaults.value(forKey: "DEVICE_TOKEN") as? String {
            return devicePassword
        } else {
            return ""
        }
    }

    static var CurrentTimestamp: String {
        //return "\(NSDate().timeIntervalSince1970 * 1000)"
        
        let date = NSDate();
        // "Apr 1, 2015, 8:53 AM" <-- local without seconds
        
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
        let defaultTimeZoneStr = formatter.string(from: date as Date);
        return defaultTimeZoneStr
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        
        print("validate emilId: \(testStr)")
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluate(with: testStr)
        
        return result
        
    }
    
    static func isValidPhoneNumber(testStr:String) -> Bool {
        
        print("validate phone: \(testStr)")
        
        let PHONE_REGEX1 = "^\\d{3}-\\d{3}-\\d{4}$"
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        
        let phoneTest1 = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX1)
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        
        let result1 =  phoneTest1.evaluate(with: testStr)
        let result =  phoneTest.evaluate(with: testStr)
        
        return result1 || result
        
    }
    
    static func isEmpty(input : String) -> Bool {
        return input.isEmpty
        
    }
    
   

    
}
