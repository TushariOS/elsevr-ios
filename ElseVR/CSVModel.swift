//
//  CSVModel.swift
//  ElseVR
//
//  Created by Astrika Mac Mini on 22/02/17.
//  Copyright © 2017 Astrika Mac Mini. All rights reserved.
//

import Foundation
public class CSVModel{
    
    public var id: String = ""
    public var image: String = ""
    public var name: String = ""
    public var author: String = ""
    public var content: String = ""
    public var time: String = ""
    public var fullImage: String = ""
    public var textTitle: String = ""
    public var backGroundImageURL: String = ""
    public var imageUpdateTiming: String = ""
    public var videoUpdateTiming: String = ""
    public var audioType: String = ""
    public var videoAvailable: String = ""
    public var youtubeLink: String = ""
    public var prevVideoUpdateTiming: String = ""
    public var prevImageUpdateTiming: String = ""
    public var resourceFilePath: String = ""
    public var videoFileSize: String = ""
    public var audioFileSize: String = ""
    public var displaySizeVideo: String = ""
    public var displaySizeAudio: String = ""
    public var audioUpdateTiming: String = ""
    public var prevAudioUpdateTiming: String = ""

    
    public required init() {
        
    }
    
    public required init(jsonNSArray:NSArray) {
        
        
        if let id = jsonNSArray[0] as? String {
            self.id = id
        }
        
        if let image = jsonNSArray[1] as? String {
            self.image = image
        }
        if let name = jsonNSArray[2] as? String {
            self.name = name
        }
        
        if let author = jsonNSArray[3] as? String {
            self.author = author
        }
        if let content = jsonNSArray[8] as? String{
            
            self.content = content
        }
        if let time = jsonNSArray[4] as? String{
            
            self.time = time
        }
        if let fullImage = jsonNSArray[5] as? String{
            
            self.fullImage = fullImage
        }
        if let textTitle = jsonNSArray[6] as? String{
            
            self.textTitle = textTitle
        }
        if let backGroundImageURL = jsonNSArray[7] as? String {
            
            self.backGroundImageURL = backGroundImageURL
            
        }
        if let imageUpdateTiming = jsonNSArray[9] as? String {
            self.imageUpdateTiming = imageUpdateTiming
        }
        
        if let videoUpdateTiming = jsonNSArray[10] as? String {
            self.videoUpdateTiming = videoUpdateTiming
        }
        if let audioType = jsonNSArray[11] as? String {
            self.audioType = audioType
        }
        
        if let videoAvailable = jsonNSArray[12] as? String {
            self.videoAvailable = videoAvailable
        }
        if let youtubeLink = jsonNSArray[13] as? String{
            
            self.youtubeLink = youtubeLink
        }
        if let prevVideoUpdateTiming = jsonNSArray[14] as? String{
            
            self.prevVideoUpdateTiming = prevVideoUpdateTiming
        }
        if let prevImageUpdateTiming = jsonNSArray[15] as? String{
            
            self.prevImageUpdateTiming = prevImageUpdateTiming
        }
        if let resourceFilePath = jsonNSArray[16] as? String{
            
            self.resourceFilePath = resourceFilePath
        }
        if let videoFileSize = jsonNSArray[17] as? String {
            
            self.videoFileSize = videoFileSize
            
        }
        if let audioFileSize = jsonNSArray[18] as? String{
            
            self.audioFileSize = audioFileSize
        }
        if let displaySizeVideo = jsonNSArray[19] as? String{
            
            self.displaySizeVideo = displaySizeVideo
        }
        if let displaySizeAudio = jsonNSArray[20] as? String{
            
            self.displaySizeAudio = displaySizeAudio
        }
        if let audioUpdateTiming = jsonNSArray[21] as? String {
            
            self.audioUpdateTiming = audioUpdateTiming
            
        }
//        if let prevAudioUpdateTiming = jsonNSArray[22] as? Double{
//            
//            self.prevAudioUpdateTiming = prevAudioUpdateTiming
//        }
        
        
    }
}
